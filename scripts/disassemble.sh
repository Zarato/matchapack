#!/bin/bash
# Author: Zarato
# Description: disassemble a file
# Note: to show the source code you have to compile with `g` flag

STYLE="monokai"
SYNTAX="intel"
LEXER="gas"

if [ $# = 2 ]; then
  # objdump -S "$1" --no-show-raw-insn -M $SYNTAX | awk -F"\n" -v RS= '/^[[:xdigit:]]+ <'"$2"'>/' | # dump function
  # perl -p -e 's/^\s+(\S+):\t//;' | # remove opcodes
  # pygmentize -l $LEXER -O style=$STYLE # syntax highlighting
  r2 -A -qc "s sym.$2; pdf" $1
else
  echo "Usage: "$0" <file> <function>"
  echo "Description: disassemble a function"
  echo "             file : input file"
  echo "             function : function to disassemble"
fi