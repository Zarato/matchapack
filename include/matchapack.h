//
// Created by Zarato on 07/02/2021.
//

#ifndef MATCHAPACK_MATCHAPACK_H
#define MATCHAPACK_MATCHAPACK_H

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

enum matcha_status {
    matcha_status_success = 0,
    matcha_status_out_of_memory = 1,
    matcha_status_error = 2
};

typedef void (*matcha_element_wise_function)(
        size_t n,
        const void *x,
        void *y
);

typedef void (*matcha_clamp_function)(
        size_t n,
        const void *x,
        void *y,
        float a,
        float b
);

struct matcha_kernels {
    uint32_t flags;
    struct {
        struct {
            matcha_element_wise_function abs;
            matcha_clamp_function clamp;
        } op;
    } f32;
};

enum matcha_status matcha_initialize(void);

enum matcha_status matcha_deinitialize(void);

struct matcha_kernels* matcha_get_kernels(void);

#ifdef __cplusplus
} // extern "C"
#endif

#endif //MATCHAPACK_MATCHAPACK_H
