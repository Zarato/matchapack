cmake_minimum_required(VERSION 3.17)

project(cpuinfo-download NONE)

include(ExternalProject)
ExternalProject_Add(cpuinfo
        GIT_REPOSITORY https://github.com/pytorch/cpuinfo.git
        GIT_TAG master
        SOURCE_DIR "${DEPENDENCIES_SOURCE_DIR}/cpuinfo"
        BINARY_DIR "${DEPENDENCIES_BINARY_DIR}/cpuinfo"
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        TEST_COMMAND ""
        )