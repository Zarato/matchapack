cmake_minimum_required(VERSION 3.17)

project(googletest-download NONE)

include(ExternalProject)
ExternalProject_Add(googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG master
        SOURCE_DIR "${DEPENDENCIES_SOURCE_DIR}/googletest"
        BINARY_DIR "${DEPENDENCIES_BINARY_DIR}/googletest"
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        TEST_COMMAND ""
        )