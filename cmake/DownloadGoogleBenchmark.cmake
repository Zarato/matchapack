cmake_minimum_required(VERSION 3.17)

project(googlebenchmark-download NONE)

include(ExternalProject)
ExternalProject_Add(googlebenchmark
        GIT_REPOSITORY https://github.com/google/benchmark.git
        GIT_TAG master
        SOURCE_DIR "${DEPENDENCIES_SOURCE_DIR}/googlebenchmark"
        BINARY_DIR "${DEPENDENCIES_BINARY_DIR}/googlebenchmark"
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        TEST_COMMAND ""
        )