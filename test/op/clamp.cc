//
// Created by Zarato on 07/02/2021.
//

#include <gtest/gtest.h>
#include <random>
#include <vector>
#include <algorithm>

#include <matchapack.h>

#define CLAMP_TEST(name, batch) \
TEST(CLAMP_OP, name) {          \
    const float a = -1.5f;      \
    const float b = 5.0f; \
    const uint32_t batch_size = batch; \
    std::random_device random_device; \
    auto rng = std::mt19937(random_device()); \
    auto f32rng = std::bind(std::uniform_real_distribution<float>(-2.0f, 6.0f), rng); \
\
    std::vector<float> input(batch_size); \
    std::vector<float> output(batch_size); \
    std::vector<float> output_true(batch_size); \
\
    std::generate(input.begin(), input.end(), std::ref(f32rng)); \
    std::fill(output.begin(), output.end(), std::nanf("")); \
\
    std::transform(input.begin(), input.end(), output_true.begin(), [&](float f){return std::fmin(std::fmax(f, a), b);}); \
\
    matcha_kernels *kernels; \
    ASSERT_EQ(matcha_status_success, matcha_initialize()); \
\
    kernels = matcha_get_kernels(); \
\
    kernels->f32.op.clamp(batch_size * sizeof(float), input.data(), output.data(), a, b); \
\
    for (size_t i = 0; i < batch_size; i++) { \
        ASSERT_EQ(output_true[i], output[i]); \
    } \
}

CLAMP_TEST(small_batch, 7)
CLAMP_TEST(medium_batch, 128)
CLAMP_TEST(large_batch, 1024*5)
CLAMP_TEST(very_large_batch, 1024*512)
CLAMP_TEST(huge_batch, 1024*1024)