# CMake file based on https://github.com/google/XNNPACK

cmake_minimum_required(VERSION 3.17)

# project
project(matchapack)

# options
set(MATCHAPACK_LIBRARY_TYPE "default" CACHE STRING "Type of library (shared, static or default) to build")
set_property(CACHE MATCHAPACK_LIBRARY_TYPE PROPERTY STRINGS default static shared)
option(MATCHAPACK_BUILD_TESTS "Build matchapack unit tests" ON)
option(MATCHAPACK_BUILD_BENCHMARKS "Build matchapack benchmarks" ON)

# get target processor
set(MATCHAPACK_TARGET_PROCESSOR "${CMAKE_SYSTEM_PROCESSOR}")
if (CMAKE_SYSTEM_NAME STREQUAL "Darwin" AND CMAKE_OSX_ARCHITECTURES MATCHES "^(x86_64|arm64|arm64e)$") # macOS support
    set(MATCHAPACK_TARGET_PROCESSOR "${CMAKE_OSX_ARCHITECTURES}")
endif()

# testing
if (MATCHAPACK_BUILD_TESTS)
    enable_testing()
endif()

# download deps
set(DEPENDENCIES_SOURCE_DIR "${CMAKE_SOURCE_DIR}/deps")
set(DEPENDENCIES_BINARY_DIR "${CMAKE_BINARY_DIR}/deps")

## install cpuinfo
if (NOT DEFINED CPUINFO_SOURCE_DIR)
    message(STATUS "Downloading cpuinfo to ${DEPENDENCIES_SOURCE_DIR}/cpuinfo")
    configure_file(cmake/DownloadCpuinfo.cmake "${DEPENDENCIES_BINARY_DIR}/cpuinfo-download/CMakeLists.txt")
    execute_process(COMMAND "${CMAKE_COMMAND}" -G "${CMAKE_GENERATOR}" .
            WORKING_DIRECTORY "${DEPENDENCIES_BINARY_DIR}/cpuinfo-download")
    execute_process(COMMAND "${CMAKE_COMMAND}" --build .
            WORKING_DIRECTORY "${DEPENDENCIES_BINARY_DIR}/cpuinfo-download")
    set(CPUINFO_SOURCE_DIR "${DEPENDENCIES_SOURCE_DIR}/cpuinfo" CACHE STRING "cpuinfo source directory")
endif()

## install google test
if (MATCHAPACK_BUILD_TESTS AND NOT DEFINED GOOGLETEST_SOURCE_DIR)
    message(STATUS "Downloading Google Test to ${DEPENDENCIES_SOURCE_DIR}/googletest")
    configure_file(cmake/DownloadGoogleTest.cmake "${DEPENDENCIES_BINARY_DIR}/googletest-download/CMakeLists.txt")
    execute_process(COMMAND "${CMAKE_COMMAND}" -G "${CMAKE_GENERATOR}" .
            WORKING_DIRECTORY "${DEPENDENCIES_BINARY_DIR}/googletest-download")
    execute_process(COMMAND "${CMAKE_COMMAND}" --build .
            WORKING_DIRECTORY "${DEPENDENCIES_BINARY_DIR}/googletest-download")
    set(GOOGLETEST_SOURCE_DIR "${DEPENDENCIES_SOURCE_DIR}/googletest" CACHE STRING "Google Test source directory")
endif()

## install google benchmark
if (MATCHAPACK_BUILD_BENCHMARKS AND NOT DEFINED GOOGLEBENCHMARK_SOURCE_DIR)
    message(STATUS "Downloading Google Benchmark to ${DEPENDENCIES_SOURCE_DIR}/googlebenchmark")
    configure_file(cmake/DownloadGoogleBenchmark.cmake "${DEPENDENCIES_BINARY_DIR}/googlebenchmark-download/CMakeLists.txt")
    execute_process(COMMAND "${CMAKE_COMMAND}" -G "${CMAKE_GENERATOR}" .
            WORKING_DIRECTORY "${DEPENDENCIES_BINARY_DIR}/googlebenchmark-download")
    execute_process(COMMAND "${CMAKE_COMMAND}" --build .
            WORKING_DIRECTORY "${DEPENDENCIES_BINARY_DIR}/googlebenchmark-download")
    set(GOOGLEBENCHMARK_SOURCE_DIR "${DEPENDENCIES_SOURCE_DIR}/googlebenchmark" CACHE STRING "Google Benchmark source directory")
endif()

set(CMAKE_CXX_STANDARD 17)

# matchapack library
set(MATCHAPACK_SRC
        src/matchapack/math.h
        src/matchapack/common.h src/init.c)

## microkernels
set(MATCHAPACK_SCALAR_MICROKERNEL_SRC)
set(MATCHAPACK_NEON_MICROKERNEL_SRC)
set(MATCHAPACK_SSE_MICROKERNEL_SRC)
set(MATCHAPACK_SSE2_MICROKERNEL_SRC)
set(MATCHAPACK_SSE3_MICROKERNEL_SRC)
set(MATCHAPACK_SSE41_MICROKERNEL_SRC)
set(MATCHAPACK_AVX_MICROKERNEL_SRC
        src/cpu/op/abs/gen/abs-avx-x8.c
        src/cpu/op/abs/gen/abs-avx-x16.c
        src/cpu/op/clamp/gen/clamp-avx-x8.c
        src/cpu/op/clamp/gen/clamp-avx-x16.c)
set(MATCHAPACK_XOP_MICROKERNEL_SRC)
set(MATCHAPACK_FMA3_MICROKERNEL_SRC)
set(MATCHAPACK_AVX2_MICROKERNEL_SRC)
set(MATCHAPACK_AVX512_MICROKERNEL_SRC
        src/cpu/op/abs/gen/abs-avx512-x16.c
        src/cpu/op/abs/gen/abs-avx512-x32.c
        src/cpu/op/clamp/gen/clamp-avx512-x16.c
        src/cpu/op/clamp/gen/clamp-avx512-x32.c)

set(MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_SCALAR_MICROKERNEL_SRC})
if (CMAKE_SYSTEM_PROCESSOR MATCHES "^armv[5-8]")
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_NEON_MICROKERNEL_SRC})
    # TODO
endif()
if (MATCHAPACK_TARGET_PROCESSOR MATCHES "^(aarch64|arm64)$")
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_NEON_MICROKERNEL_SRC})
    # TODO
endif()
if (MATCHAPACK_TARGET_PROCESSOR MATCHES "^(i[3-6]86|x86_64|AMD64)$")
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_SSE_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_SSE2_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_SSE3_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_SSE41_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_AVX_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_XOP_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_FMA3_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_AVX2_MICROKERNEL_SRC})
    list(APPEND MATCHAPACK_MICROKERNEL_SRC ${MATCHAPACK_AVX512_MICROKERNEL_SRC})
endif()

# type of library
if (MATCHAPACK_LIBRARY_TYPE STREQUAL "default")
    add_library(matchapack ${MATCHAPACK_SRC} ${MATCHAPACK_MICROKERNEL_SRC})
elseif(MATCHAPACK_LIBRARY_TYPE STREQUAL "shared")
    add_library(matchapack SHARED ${MATCHAPACK_SRC} ${MATCHAPACK_MICROKERNEL_SRC})
elseif (MATCHAPACK_LIBRARY_TYPE STREQUAL "static")
    add_library(matchapack STATIC ${MATCHAPACK_SRC} ${MATCHAPACK_MICROKERNEL_SRC})
else()
    message(FATAL_ERROR "Unsupported library type \"${MATCHAPACK_LIBRARY_TYPE}\". Must be default, shared or static.")
endif()

# TODO : building
set_target_properties(matchapack PROPERTIES C_STANDARD 99 C_EXTENSIONS YES)
# TODO : arm

## x86-64
if (MATCHAPACK_TARGET_PROCESSOR MATCHES "^(i[3-6]86|x86|x86_64|AMD64)$")
    if (MSVC)
        if (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86" OR CMAKE_SIZEOF_VOID_P EQUAL 4)
            # SSE
            set_property(SOURCE ${MATCHAPACK_SSE_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:SSE ")
            set_property(SOURCE ${MATCHAPACK_SSE2_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:SSE2 ")
            set_property(SOURCE ${MATCHAPACK_SSSE3_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:SSE2 ")
            set_property(SOURCE ${MATCHAPACK_SSE41_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:SSE2 ")
        endif()
        set_property(SOURCE ${MATCHAPACK_AVX_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:AVX ")
        set_property(SOURCE ${MATCHAPACK_XOP_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:AVX ")
        set_property(SOURCE ${MATCHAPACK_FMA3_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:AVX ")
        set_property(SOURCE ${MATCHAPACK_AVX2_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:AVX2 ")
        set_property(SOURCE ${MATCHAPACK_AVX512_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /arch:AVX512 ")
    else()
        set_property(SOURCE ${MATCHAPACK_SSE_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -msse ")
        set_property(SOURCE ${MATCHAPACK_SSE2_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -msse2 ")
        set_property(SOURCE ${MATCHAPACK_SSSE3_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -mssse3 ")
        set_property(SOURCE ${MATCHAPACK_SSE41_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -msse4.1 ")
        set_property(SOURCE ${MATCHAPACK_AVX_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -mavx ")
        set_property(SOURCE ${MATCHAPACK_XOP_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -mxop ")
        set_property(SOURCE ${MATCHAPACK_FMA3_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -mfma ")
        set_property(SOURCE ${MATCHAPACK_AVX2_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -mfma -mavx2 ")
        set_property(SOURCE ${MATCHAPACK_AVX512_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -mavx512f -mavx512cd -mavx512bw -mavx512dq -mavx512vl ")
    endif()
endif()

if (MSVC)
    target_compile_definitions(matchapack PRIVATE "restrict=")
    if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
        set_property(SOURCE ${MATCHAPACK_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /O2 ")
        set_property(SOURCE ${MATCHAPACK_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " /O2 ")
    endif()
else()
    if (NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
        set_property(SOURCE ${MATCHAPACK_MICROKERNEL_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -O2 ")
        set_property(SOURCE ${MATCHAPACK_SRC} APPEND_STRING PROPERTY COMPILE_FLAGS " -Os ")
    endif()
endif()

target_include_directories(matchapack PUBLIC include)
target_include_directories(matchapack PRIVATE src)
set_target_properties(matchapack PROPERTIES PUBLIC_HEADER include/matchapack.h)

# Find libm
find_library(LIBM m)
if (LIBM)
    target_link_libraries(matchapack PRIVATE ${LIBM})
endif()

# Configure cpuinfo
if (NOT TARGET cpuinfo)
    set(CPUINFO_BUILD_TOOLS OFF CACHE BOOL "")
    set(CPUINFO_BUILD_UNIT_TESTS OFF CACHE BOOL "")
    set(CPUINFO_BUILD_MOCK_TESTS OFF CACHE BOOL "")
    set(CPUINFO_BUILD_BENCHMARKS OFF CACHE BOOL "")
    add_subdirectory("${CPUINFO_SOURCE_DIR}" "${DEPENDENCIES_BINARY_DIR}/cpuinfo")
endif()
target_link_libraries(matchapack PRIVATE cpuinfo)

install(TARGETS matchapack
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_LIBDIR})

# unit tests
if (MATCHAPACK_BUILD_TESTS)
    message(STATUS "Building tests...")
    if (NOT TARGET gtest)
        set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
        add_subdirectory("${GOOGLETEST_SOURCE_DIR}" "${DEPENDENCIES_BINARY_DIR}/googletest")
    endif()

    # build unit tests

    ## abs
    add_executable(abs-test test/op/abs.cc)
    set_target_properties(abs-test PROPERTIES
            CXX_STANDARD 11
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO)
    target_include_directories(abs-test PRIVATE src test)
    target_link_libraries(abs-test PRIVATE matchapack cpuinfo gtest gtest_main)
    add_test(abs-test abs-test)

    ## clamp
    add_executable(clamp-test test/op/clamp.cc)
    set_target_properties(clamp-test PROPERTIES
            CXX_STANDARD 11
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO)
    target_include_directories(clamp-test PRIVATE src test)
    target_link_libraries(clamp-test PRIVATE matchapack cpuinfo gtest gtest_main)
    add_test(clamp-test clamp-test)
endif()

# benchmarks
if (MATCHAPACK_BUILD_BENCHMARKS)
    # TODO : benchmarks
endif()