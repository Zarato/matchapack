<!-- PROJECT LOGO -->
<br/>
<p align="center">
    <h1 align="center">MatchaPack</h1>
    <p align="center">A lightweight optimized and well-documented library for machine learning. </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#dependencies">Dependencies</a></li>
    <li><a href="#supported-devices">Supported devices</a></li>
    <li><a href="#useful-resources">Useful resources</a></li>
    <li><a href="#progress">Progress</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About the project
With the significant rise in the interest in machine learning, the frameworks
have to be more and more performant. To solve this problem, frameworks such as
[TensorFlow](https://www.tensorflow.org/?hl=fr) and [PyTorch](https://pytorch.org/)
use highly performant libraries such as [XNNPACK](https://github.com/google/XNNPACK).

For educational purposes I have decided to write my own library. My final goal
is to write a lightweight framework which only does prediction (not training).
I want to write this framework because I want to learn more about the world of software
optimization and the backend of machine learning frameworks (that is to say, the way that the frameworks
are written).

In order to achieve maximum performance, I make maximum use of the vectorisation
operations offered by the CPUs such as:
- [SSE](https://fr.wikipedia.org/wiki/Streaming_SIMD_Extensions)
- [AVX](https://fr.wikipedia.org/wiki/Advanced_Vector_Extensions)
- [AVX-512](https://en.wikipedia.org/wiki/AVX-512)
- [NEON](https://fr.wikipedia.org/wiki/ARM_NEON)

I also want to write some operations with [CUDA](https://fr.wikipedia.org/wiki/Compute_Unified_Device_Architecture)
to take full advantage of the GPU architecture to perform convolution operations
that are very fast on it.

<!-- GETTING STARTED -->
## Getting started
TODO

<!-- USAGE -->
## Usage
TODO

<!-- CONTRIBUTING -->
## Contributing
TODO

<!-- LICENSE -->
## License
Distributed under the [MIT](https://choosealicense.com/licenses/mit/) License.
See `LICENSE.md` for more information.

<!-- DEPENDENCIES -->
## Dependencies
- [cpuinfo](https://github.com/pytorch/cpuinfo)
- [Google Test](https://github.com/google/googletest)
- [Google Benchmark](https://github.com/google/benchmark)

<!-- SUPPORTED DEVICES -->
## Supported devices
I am currently working on it so there are some devices which are not supported.
- [x] Linux
- [x] Windows
- [ ] OSX
- [ ] iOS
- [ ] Android

<!-- USEFUL RESOURCES -->
## Useful resources
In order to write this library I use some useful resources that I share with you
- [Intel Intrinsics](https://software.intel.com/sites/landingpage/IntrinsicsGuide/)
is very useful when you are writing a library with vectorised operations for x86.
Instructions for MMX, SSE/SSE2/SSE3/SSE3/SSE4.1/SSE4.2, AVX/AVX2/AVX512, FMA are documented with a small 
  pseudo-code for each instruction.
- [Neon Intrinsics](https://developer.arm.com/architectures/instruction-sets/simd-isas/neon/intrinsics)
is useful for writing ARM library with vectorised operations.
  
<!-- PROGRESS -->
## Progress
Currently the library is under writing and cannot be used for production.
But I try to work on it on my free time (because I am still a student and
I have stuff to do...).

However, if you want to check what will implement the library, check `TODO.md`.
I will try to look for a tool (such as a visual TODO list) to specify
the progress of the project, and fill in the implemented functions as well
as the supported architectures.