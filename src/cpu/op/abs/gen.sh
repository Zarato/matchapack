#!/bin/bash

# change working directory
# cd "${0%/*}"

MATCHAGEN=tools/matchagen.py
PYTHON=python

if [[ "$(python3 --version)" =~ "Python 3" ]]; then
  PYTHON=python3
fi

# AVX 256-bits
BATCH=(8 16)
for i in "${BATCH[@]}"; do
  $PYTHON $MATCHAGEN src/cpu/op/abs/avx.c.in -D BATCH="$i" -o "src/cpu/op/abs/gen/abs-avx-x$i.c"
done

# AVX 512-bits
BATCH=(16 32)
for i in "${BATCH[@]}"; do
  $PYTHON $MATCHAGEN src/cpu/op/abs/avx512.c.in -D BATCH="$i" -o "src/cpu/op/abs/gen/abs-avx512-x$i.c"
done