$assert BATCH % 16 == 0
$assert BATCH >= 16
#include <assert.h>
#include <stdint.h>

#include <immintrin.h>

#include <matchapack/math.h>
#include <matchapack/common.h>
#include <matchapack/op.h>

/**
 * Clamp all elements of the source vector into the range [#min, #max].
 *
 * @note This function is available only if you are compiling with AVX support.
 *
 * @param[in] n Size of the vector
 * @param[in] x Input vector
 * @param[out] y Output vector
 * @param[in] a Minimum value
 * @param[in] b Maximum value
 */
void matcha_f32_clamp__avx512_x${BATCH}(
    size_t n,
    const float *x,
    float *y,
    float a,
    float b
)
{
    assert(n != 0);
    assert(n % sizeof(float) == 0); // n must be a multiple of sizeof(float)
    assert(x != NULL);
    assert(y != NULL);

    const __m512 va = _mm512_set1_ps(a);
    const __m512 vb = _mm512_set1_ps(b);

    for (; n >= ${BATCH} * sizeof(float); n -= ${BATCH} * sizeof(float)) {
        $for i in range(0, BATCH, 16):
            __m512 x${i} = _mm512_loadu_ps(x + ${i});
        x += ${BATCH};

        $for i in range(0, BATCH, 16):
            x${i} = _mm512_max_ps(x${i}, va);
        $for i in range(0, BATCH, 16):
            x${i} = _mm512_min_ps(x${i}, vb);

        $for i in range(0, BATCH, 16):
            _mm512_storeu_ps(y + ${i}, x${i});
        y += ${BATCH};
    }

    $if BATCH > 16:
        for (; n >= 16 * sizeof(float); n -= 16 * sizeof(float)) {
            __m512 x0 = _mm512_loadu_ps(x);
            x0 = _mm512_max_ps(x0, va);
            x0 = _mm512_min_ps(x0, vb);
            _mm512_storeu_ps(y, x0);
            x += 16; y += 16;
        }

    if MATCHA_TRUE(n != 0) {
        assert(n >= sizeof(float));
        assert(n <= 15 * sizeof(float));

        // build a mask from the value of n
        // Method:
        // - n is a multiple of sizeof(float), for example n = 5 * sizeof(float) = 20
        // - divide n by sizeof(float)=4, which is equivalent to shifting 2 bits to the right (n >>= 2)
        // - compute x = 1 << n (here only the sixth bit is set)
        // - compute y = x - 1 (here only the first five bits are set)
        // for n = 5, y = 00000000000000000000000000011111 (uint32_t)
        n >>= 2;
        const __mmask16 mask = (__mmask16)((uint16_t)((uint32_t)(1 << n) - 1)); // need a __mmask16... well
        __m512 x0 = _mm512_maskz_loadu_ps(mask, x);
        x0 = _mm512_max_ps(x0, va);
        x0 = _mm512_min_ps(x0, vb);
        _mm512_mask_storeu_ps(y, mask, x0);

    }

}