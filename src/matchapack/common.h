//
// Created by Zarato on 06/02/2021.
//

#ifndef MATCHAPACK_COMMON_H
#define MATCHAPACK_COMMON_H

// Indicates that matchapack is initialized
#define MATCHA_INIT_FLAG 1

#if defined(__GNUC__)
    #define MATCHA_EXPECT(cond, val) (__builtin_expect(!!(cond), val)) // https://stackoverflow.com/questions/7346929/what-is-the-advantage-of-gccs-builtin-expect-in-if-else-statements
#else
    #define MATCHA_EXPECT(cond, val) (!!(cond))
#endif

#define MATCHA_TRUE(cond) MATCHA_EXPECT(cond, 1)
#define MATCHA_FALSE(cond) MATCHA_EXPECT(cond, 0)


#endif //MATCHAPACK_COMMON_H
