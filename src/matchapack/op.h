//
// Created by Zarato on 07/02/2021.
//

#ifndef MATCHAPACK_OP_H
#define MATCHAPACK_OP_H

#include <stdint.h>
#include <stddef.h>
#include <matchapack/common.h>

#ifdef __cplusplus
extern "C" {
#endif

#define ABS_F32_FUNCTION(name) \
    void name(                 \
        size_t n,              \
        const float *x,        \
        float *y               \
    );

ABS_F32_FUNCTION(matcha_f32_abs__avx512_x16)
ABS_F32_FUNCTION(matcha_f32_abs__avx512_x32)
ABS_F32_FUNCTION(matcha_f32_abs__avx_x8)
ABS_F32_FUNCTION(matcha_f32_abs__avx_x16)

#define CLAMP_F32_FUNCTION(name)\
    void name(                  \
        size_t n,               \
        const float *x,         \
        float *y,               \
        float a,                \
        float b                 \
    );

CLAMP_F32_FUNCTION(matcha_f32_clamp__avx512_x16)
CLAMP_F32_FUNCTION(matcha_f32_clamp__avx512_x32)
CLAMP_F32_FUNCTION(matcha_f32_clamp__avx_x8)
CLAMP_F32_FUNCTION(matcha_f32_clamp__avx_x16)

#ifdef __cplusplus
} // extern "C"
#endif

#endif //MATCHAPACK_OP_H
