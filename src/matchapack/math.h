//
// Created by Zarato on 06/02/2021.
//

#ifndef MATCHAPACK_MATH_H
#define MATCHAPACK_MATH_H

#include <stdint.h>

inline static float nonsign_mask_f32() {
#if defined(__INTEL_COMPILER)
    return _castu32_f32(0x7FFFFFFF); // from https://software.intel.com/sites/landingpage/IntrinsicsGuide/#cats=Cast&expand=600
#elif defined(__GNUC__)
    return __builtin_nanf("0x7FFFFFFF"); // from https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html
#else
    union {
        uint32_t word_repres;
        float float_repres;
    } f;
    f.word_repres = 0x7FFFFFFF;
    return f.float_repres;
#endif
}

#endif //MATCHAPACK_MATH_H
