//
// Created by Zarato on 07/02/2021.
//

#include <stdint.h>
#include <cpuinfo.h>

#include <matchapack.h>
#include <matchapack/op.h>
#include <matchapack/common.h>


struct matcha_kernels kernels = {0};

static void init(void) {
    uint32_t flags = 0;
    if (cpuinfo_has_x86_avx512f()) {
        kernels.f32.op.abs = (matcha_element_wise_function)matcha_f32_abs__avx512_x16;
        kernels.f32.op.clamp = (matcha_clamp_function)matcha_f32_clamp__avx512_x16;
    } else if (cpuinfo_has_x86_avx()) {
        kernels.f32.op.abs = (matcha_element_wise_function)matcha_f32_abs__avx_x16;
        kernels.f32.op.clamp = (matcha_clamp_function)matcha_f32_clamp__avx_x16;
    } else {
        // TODO : SSE
    }
    flags |= MATCHA_INIT_FLAG;
    kernels.flags = flags;
}

enum matcha_status matcha_initialize(void) {
    if (!cpuinfo_initialize()) {
        return matcha_status_out_of_memory;
    }

    if ((kernels.flags & MATCHA_INIT_FLAG) == 0) {
        init();
    }
    return matcha_status_success;
}

enum matcha_status matcha_deinitialize(void) {
    cpuinfo_deinitialize();
    return matcha_status_success;
}

struct matcha_kernels* matcha_get_kernels(void) {
    return &kernels;
}